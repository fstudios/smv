# Saumausmassavalitsin theme
A WordPress theme for www.saumausmassavalitsin.fi

Setup
------------------------------------

1. npm install
2. npm run build
3. Go to Appearances > Themes in your WordPress Dashboard and Select 'Saumausmassavalitsin'
4. Go to Settings > Permalinks and click "Save"

Development
------------------------------------

1. npm run watch
