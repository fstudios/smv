<?php

/**
 * Remove menu pages from WP admin
 */
function smv_remove_admin_menu_pages() {
	// Hide posts menu
    remove_menu_page( 'edit.php' );
}
add_action( 'admin_menu', 'smv_remove_admin_menu_pages' );
