<?php

/**
 * Register Position post type
 */
function smv_add_position_post_type() {

	register_post_type( 'smv_position',
		array(
			'labels' => array(
				'name' => __( 'Kohteet' ),
				'singular_name' => __( 'Kohde' )
			),
			'public' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-admin-page',
			'rewrite' => array(
				'slug' => 'position'
			),
			'supports' => array('title'),
			'taxonomies' => array('category'),
			'show_in_rest' => true,
			'menu_position' => 5
		)
	);
}

add_action( 'init', 'smv_add_position_post_type', 1 );
