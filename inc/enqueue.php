<?php

/**
 * Enqueue scripts and styles.
 */
function smv_scripts() {

	/*
		Load stylesheets
	*/
	wp_enqueue_style( 'bootstrap-style', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css' );
	wp_enqueue_style( 'roboto', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500' );
	wp_enqueue_style( 'smv-style', get_stylesheet_directory_uri() . '/dist/style.css', array(), '1.0.1' );

    /*
		Load scripts
	*/
    wp_enqueue_script( 'smv-script', get_stylesheet_directory_uri() . '/dist/app.js', array(), '1.0.1', true );

	$url = trailingslashit( home_url() );
	$path = trailingslashit( parse_url( $url, PHP_URL_PATH ) );
	$theme = trailingslashit( get_template_directory_uri() );

	$root = esc_url_raw( $url );
	$api = $root . 'wp-json/wp/v2/';

	wp_scripts()->add_data( 'smv-script', 'data', sprintf( 'var smv_config = %s;', wp_json_encode( array(
			'title' => get_bloginfo( 'name', 'display' ),
			'path' => $path,
			'URL' => array(
				'api' => $api,
				'root' => $root,
				'theme' => $theme
			 )
		)
	)));
}
add_action( 'wp_enqueue_scripts', 'smv_scripts' );
