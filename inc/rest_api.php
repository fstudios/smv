<?php

/**
 * Add meta fields to JSON output
 */
function smv_register_fields() {

	/*
		Featured Image
	*/
	register_rest_field( 'category',
		'category_image_src',
		array(
			'get_callback'		=> 'smv_get_image_src',
			'update_callback'	=> null,
			'schema'			=> null
		)
    );

	/*
		Featured Image URL
	*/
	register_rest_field( 'category',
		'category_list_image_src',
		array(
			'get_callback'		=> 'smv_get_list_image_src',
			'update_callback'	=> null,
			'schema'			=> null
		)
    );

	/*
		Position top location
	*/
	register_rest_field( 'smv_position',
		'location_top',
		array(
			'get_callback'		=> 'smv_get_location_top',
			'update_callback'	=> null,
			'schema'			=> null
		)
    );

	/*
		Position left location
	*/
	register_rest_field( 'smv_position',
		'location_left',
		array(
			'get_callback'		=> 'smv_get_location_left',
			'update_callback'	=> null,
			'schema'			=> null
		)
    );

	/*
		Position base URL
	*/
	register_rest_field( 'smv_position',
		'base_url',
		array(
			'get_callback'		=> 'smv_get_base_url',
			'update_callback'	=> null,
			'schema'			=> null
		)
    );

	/*
		Position reverse text UI
	*/
	register_rest_field( 'smv_position',
		'reverse',
		array(
			'get_callback'		=> 'smv_get_reverse',
			'update_callback'	=> null,
			'schema'			=> null
		)
    );

	/*
		Position location links
	*/
	register_rest_field( 'smv_position',
		'location_links',
		array(
			'get_callback'		=> 'smv_get_location_links',
			'update_callback'	=> null,
			'schema'			=> null
		)
    );
}
add_action( 'rest_api_init', 'smv_register_fields' );


/*
	Get category image
*/
function smv_get_image_src( $object, $field_name, $request ) {
    return get_term_meta( $object['id'], SMV_PREFIX . 'category_image', true);
}

/*
	Get category list image
*/
function smv_get_list_image_src( $object, $field_name, $request ) {
    return get_term_meta( $object['id'], SMV_PREFIX . 'category_list_image', true);
}

/*
	Get location top position
*/
function smv_get_location_top( $object, $field_name, $request ) {
    return esc_html( get_post_meta( $object['id'], SMV_PREFIX . 'top_position', true ) );
}

/*
	Get location top position
*/
function smv_get_location_left( $object, $field_name, $request ) {
    return esc_html( get_post_meta( $object['id'], SMV_PREFIX . 'left_position', true ) );
}

/*
	Get location top position
*/
function smv_get_base_url( $object, $field_name, $request ) {
    return esc_url( get_post_meta( $object['id'], SMV_PREFIX . 'base_url', true ) );
}

/*
	Get reverse setting
*/
function smv_get_reverse( $object, $field_name, $request ) {
    return (bool) get_post_meta( $object['id'], SMV_PREFIX . 'reverse', true );
}

/*
	Get location links
*/
function smv_get_location_links( $object, $field_name, $request ) {
    return get_post_meta( $object['id'], SMV_PREFIX . 'links', true );
}
