<?php

/**
 * Create CMB2 metabox for Position post type
 */
function smv_position_metabox_register() {

	$cmb_position = new_cmb2_box( array(
		'id'               => SMV_PREFIX . 'position_fields',
		'title'            => esc_html__( 'Pinnat', 'cmb2' ),
		'object_types'     => array( 'post', 'smv_position' ),
	) );

	$cmb_position->add_field( array(
		'name' => esc_html__( 'Top Position', 'cmb2' ),
		'desc' => 'Sijainti pikseleina tai prosentteina, esim. 50% or 100px',
		'id'   => SMV_PREFIX . 'top_position',
		'type' => 'text',
	) );

	$cmb_position->add_field( array(
		'name' => esc_html__( 'Left Position', 'cmb2' ),
		'desc' => 'Sijainti pikseleina tai prosentteina, esim. 50% or 100px',
		'id'   => SMV_PREFIX . 'left_position',
		'type' => 'text',
	) );

	$cmb_position->add_field( array(
		'name' => esc_html__( 'Base URL', 'cmb2' ),
		'desc' => 'Verkko-osoite, ilman loppupäätettä.',
		'id'   => SMV_PREFIX . 'base_url',
		'type' => 'text_url',
	) );

	$cmb_position->add_field( array(
		'name' => 'Teksti vasemmalla puolella',
		'id'   => SMV_PREFIX . 'reverse',
		'type' => 'checkbox',
	) );

	$position_group = $cmb_position->add_field( array(
		'id'          => SMV_PREFIX . 'links',
		'type'        => 'group',
		'description' => __( 'Tartuntapinnat URL-osoitteineen', 'cmb2' ),
		'options'     => array(
			'group_title'   => __( 'Tartuntapinta', 'cmb2' ),
			'add_button'    => __( 'Lisää ', 'cmb2' ),
			'remove_button' => __( 'Poista', 'cmb2' ),
			'sortable'      => true,
		),
	) );

	$cmb_position->add_group_field( $position_group, array(
		'name' => 'Nimi',
		'id'   => SMV_PREFIX . 'link_title',
		'type' => 'text',
	) );

	$cmb_position->add_group_field( $position_group, array(
		'name' => 'Koodi',
		'id'   => SMV_PREFIX . 'link_code',
		'type' => 'text',
	) );
}

add_action( 'cmb2_init', 'smv_position_metabox_register' );


/**
 * Create CMB2 metabox for category image
 */
function smv_category_metabox_register() {

	$cmb_category = new_cmb2_box( array(
		'id'               => SMV_PREFIX . 'category_fields',
		'title'            => esc_html__( 'Category Metabox', 'cmb2' ),
		'object_types'     => array( 'term' ),
		'taxonomies'       => array( 'category' ),
	) );

	$cmb_category->add_field( array(
		'name' => esc_html__( 'Category Image', 'cmb2' ),
		'id'   => SMV_PREFIX . 'category_image',
		'type' => 'file',
		'text'    => array(
			'add_upload_file_text' => 'Add Image'
		),
	) );

	$cmb_category->add_field( array(
		'name' => esc_html__( 'Category List Image', 'cmb2' ),
		'id'   => SMV_PREFIX . 'category_list_image',
		'type' => 'file',
		'text'    => array(
			'add_upload_file_text' => 'Add Image'
		),
	) );
}

add_action( 'cmb2_init', 'smv_category_metabox_register' );

