<?php

define( 'SMV_PLUGIN_DIR', dirname(__FILE__) );
define( 'SMV_PLUGIN_URL', plugin_dir_url(__FILE__) );
define( 'SMV_PREFIX', 'smv_' );

require_once SMV_PLUGIN_DIR . '/cmb2/init.php';
require_once SMV_PLUGIN_DIR . '/inc/load.php';
