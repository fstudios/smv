import React from 'react'
import { Link } from 'react-router-dom'

const headerLogo = smv_config.URL.theme + 'img/header-logo.jpg'
const backIcon = smv_config.URL.theme + 'img/back-icon.png'

const goHome = ( e ) => {
	e.preventDefault()

	let redirect = window.location.origin
	window.location.assign( redirect )
}

const goBack = ( e ) => {
	e.preventDefault()

	let paths = _.compact( window.location.pathname.split( '/' ) )
	paths.pop()

	let redirect = [window.location.origin].concat( paths ).join( '/' )
	window.location.assign( redirect )
}

const Header = () => {
	const isHomepage = ( window.location.pathname === '/' )

	return (
	    <div className="container-fluid">
			<div className="row">
				<div className="col-12 header-top">
					{ !isHomepage &&
						<a href="#" onClick={ goBack } className="back-icon">
							<img src={ backIcon } alt="" />
						</a>
					}
					<Link to={ smv_config.path } onClick={ goHome }>
						<img src={ headerLogo } className="header-logo" alt="" />
					</Link>
				</div>
			</div>
			<div className="row">
				<div className="col-12 header-bottom">
					Valitse saumauksen kohde:
				</div>
			</div>
	    </div>
	)
}

export default Header