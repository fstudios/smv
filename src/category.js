import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Transition from 'react-transition-group/Transition'
import InfoModal from './info-modal'
import slug from 'slug'
import API from './api'
import _ from 'lodash'

const duration = 10
const offset = 0

const defaultStyles = { 
	transition: 'all 600ms ease-in-out', 
	opacity: 0, 
	position: 'relative', 
	'top': offset, 
	'transform': 'scale(1.015)'
}

const enterStyles = Object.assign( {}, defaultStyles, { 
	opacity: 1, 
	'top': 0, 
	'transform': 'scale(1.0)'
} )

const transitionStyles = {
	exited: defaultStyles,
	exiting: defaultStyles,
	entering: defaultStyles,
	entered: enterStyles
}

class Category extends React.Component {

	constructor( props ) {
		super( props )

		this.state = {
			categories: this.props.categories,
			positions: [],
			selected: false,
			location: null,
			page: 0,
			modal: false,
			controller: false,
			fadeIn: false
		}

		this.showModal = this.showModal.bind( this )
		this.onCloseModal = this.onCloseModal.bind( this )
		this.clickLink = this.clickLink.bind( this )
		this.clickSurface = this.clickSurface.bind(this)
	}

	componentDidMount() {
		const category = _.first( this.props.categories )

		ga('send', {
			hitType: 'pageview',
			page: window.location.pathname
		});

		this.setState({
			fadeIn: true
		})

		// Fetch positions for the category
		API.getPositions( category.id )
			.then( ( res ) => {
				if ( res.length > 0 ) {
					this.setState({
						positions: res
					})
				}
			})
	}

	decodeHtml( html ) {
	    var txt = document.createElement("textarea");
	    txt.innerHTML = html;
	    return txt.value;
	}

	showModal( e ) {
		const target = e.currentTarget
		const id = target.getAttribute( 'data-id' )

		const title = this.decodeHtml( target.getAttribute( 'data-title' ) )

		const location = slug( title, {
		    lower: true
		})

		const path = `${ window.location.pathname }/${ location }`

		ga('send', {
			hitType: 'pageview',
			page: path
		});

		this.setState({
			modal: true,
			selected: id,
			location: location
		})
	}

	onCloseModal() {
		this.setState({
			modal: false,
			selected: false,
			location: null
		})
	}

	clickLink( e ) {
		e.preventDefault()
	}

	clickSurface( e ) {
		const title = e.currentTarget.getAttribute( 'data-title' )

		const name = slug( title, {
			lower: true
		})

		const path = `${ window.location.pathname }/${ this.state.location }/${ name }`

		ga('send', {
			hitType: 'pageview',
			page: path
		});
	}

	render() {
		let _this = this
		let links = []

		const categories = this.props.categories
		const positions = this.state.positions

		const isLoading = !this.state.categories.length
		const loaderURL = smv_config.URL.theme + 'img/loading-icon-white.gif'
		const hotspotURL = smv_config.URL.theme + 'img/hotspot.png'

		// Position links
		if ( this.state.selected ) {

			let position = _.find( this.state.positions, function( item ) {
				return item.id == _this.state.selected
			})

			links = position.location_links.map( ( link ) => {
				return {
					link_url: position.base_url + link.smv_link_code,
					link_title: link.smv_link_title
				}
			})
		}

		return (
			<div>
				<Transition in={ this.state.fadeIn } timeout={ duration }>
					{ ( state ) => (
						<div className="category-container single" style={ transitionStyles[state] }>
							{ categories.map( ( item, index ) => {
								return (
									<Link key={ index } 
										to={ `/category/${ item.slug }` }
										onClick={ _this.clickLink } 
										className="vertical-layout-link" 
										style={{ backgroundImage: 'url(' + item.category_image_src + ')' }}>
										<img className="inner-image" src={item.category_image_src} alt="" />
										<div className="vertical-layout-text single">{ item.name }</div>
										{ positions.map( ( item, index ) => {
											let className = item.reverse ? 'category-position reverse' : 'category-position'

											return (
												<div key={ index }
													className={ className }
													data-id={ item.id }
													data-title={ item.title.rendered }
													onClick={ _this.showModal }
													style={{
														top: item.location_top,
														left: item.location_left,
														backgroundImage: 'url(' + hotspotURL + ')'
													}}
													dangerouslySetInnerHTML={{ __html: item.title.rendered }}
												></div>
											)
										})}
									</Link>
								)
							})}
							<InfoModal type="list" isOpen={ _this.state.modal } onClose={ _this.onCloseModal }>
								<div className="modal-content-description">
									<p>Valitse vielä tartuntapinta niin ohjaamme sinut suoraan sopivien tuotteittemme luokse <a href="https://saumaustarvike.fi/verkkokauppa" target="_blank">verkkokauppaan</a>:</p>
								</div>
								<div className="modal-content-container">
									<ul>
										{ links.map( ( item, index ) => {
											return (
												<li key={ index }>
													<a href={ item.link_url } 
														className="surface-link" 
														onClick={ _this.clickSurface }
														data-title={ item.link_title }
														target="_blank">
														<span className="name">
															{ item.link_title }
														</span>
														<span className="chevron"></span>
													</a>
												</li>
											)
										})}
									</ul>
								</div>
							</InfoModal>
						</div>
					)}
				</Transition>
			</div>
		)
	}
}

export default Category
