import React from 'react'
import { Modal, ModalHeader, ModalBody } from 'reactstrap'

class InfoModal extends React.Component {

	constructor( props ) {
		super( props )

		this.state = {
			isOpen: this.props.isOpen,
			activeTab: '1'
		}

		this.closeModal = this.closeModal.bind( this )
	}

	closeModal(e) {
		e.preventDefault()

		this.props.onClose()

		this.setState({
			isOpen: false
		})
	}

	shouldComponentUpdate( nextProps ) {
		return true
	}

	componentDidUpdate( prevProps, prevState ) {

		// Set from parent component
		if ( prevProps.isOpen !== this.props.isOpen ) {
			this.setState({
				isOpen: this.props.isOpen
			})
		}
	}

	render() {
		let className =  'information-modal type-' + this.props.type

		return (
			<Modal isOpen={ this.state.isOpen }
					size="lg"
					fade={ true }
					className={ className }
					modalTransition={{ timeout: 0 }}
					backdropTransition={{ timeout: 0 }}>
				<div className="modal-header-container">
					<div className="modal-close-container">
						<div className="modal-close-btn" onClick={ this.closeModal }>
							Sulje X
						</div>
					</div>
				</div>
				<ModalBody>
					{ this.props.children }
				</ModalBody>
			</Modal>
		)
	}
}

export default InfoModal
