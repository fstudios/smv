
const getRequest = ( url ) => {

    const $d = new Promise(( resolve, reject ) => {

		fetch( smv_config.URL.api + url )
			.then( res => {
				if ( !res.ok ) {
					throw Error( res.statusText )
				}
				return res.json()
			})
			.then( res => {
				resolve( res )
			})
			.catch( err => {
				console.log( 'Error in fetch operation', err )
				reject( err )
			})
	})

    return $d
}

const API = {

	getParentCategories: () => {
		const parentsPath = 'categories?parent=0&slug=ulkotilat,sisatilat'
		return getRequest( parentsPath )
	},

	getCategory: ( slug ) => {
		const categoryPath = 'categories?slug=' + slug
		return getRequest( categoryPath )
	},

	getSubCategories: ( id ) => {
		const categoriesPath = 'categories?parent=' + id
		return getRequest( categoriesPath )
	},

	getPositions( id ) {
		let positionPath = 'smv_position?categories=' + id
		return getRequest( positionPath )
	}
}

export default API