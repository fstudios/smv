import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import { Link } from 'react-router-dom'
import Transition from 'react-transition-group/Transition'
import Cookies from 'universal-cookie'
import API from './api'
import _ from 'lodash'

import Header from './header'
import Footer from './footer'
import Category from './category'
import CategoryList from './category-list'
import Preload from './preload'

import { confirmAlert } from 'react-confirm-alert'
import 'react-confirm-alert/src/react-confirm-alert.css'

const cookies = new Cookies()
const rotateURL = smv_config.URL.theme + 'img/repeat.svg'
const loaderURL = smv_config.URL.theme + 'img/loader-icon-transparent.gif'
const duration = 150

class Main extends Component {

	constructor( props ) {
		super( props )

		const preloading = !cookies.get( 'preloaded' )
		const animated = !preloading

		this.state = {
			preloading: preloading,
			animated: animated,
			loading: false,
			fadeIn: false,
			parents: [],
			categories: [],
			category: [],
			view: false
		}

		this.navigate = this.navigate.bind( this )
		this.getParentCategories = this.getParentCategories.bind( this )
		this.getCategoryList= this.getCategoryList.bind( this )
		this.getCategory = this.getCategory.bind( this )
		this.goToSubpage = this.goToSubpage.bind( this )
		this.goToList = this.goToList.bind( this )
		this.preloadComplete = this.preloadComplete.bind( this )
		this.orientationListener = this.orientationListener.bind( this )
		this.onBrowserNavigation = this.onBrowserNavigation.bind( this )
	}

	componentWillUnmount() {
		this.getParentCategories = null
		this.getCategoryList = null
		this.getCategory = null
	}

	componentDidMount() {
		const { parent, category } = this.props.match.params
		this.navigate( category, parent );

		this.orientationListener();

		window.onpopstate = this.onBrowserNavigation;
	}

	onBrowserNavigation( e, f ) {
		e.preventDefault();
		
		const { parent, category } = this.props.match.params
		this.navigate( category, parent );
	}

	navigate( category, parent ) {
		if ( category ) {
			this.getCategory( category )
			this.setState({
				view: 'category',
				fadeIn: true
			})

		} else if ( parent ) {
			this.getCategoryList( parent )
			this.setState({
				view: 'list',
				fadeIn: true
			})

		} else {
			this.getParentCategories()
			this.setState({
				view: 'home',
				fadeIn: true
			})
		}
	}

	getParentCategories() {
		API.getParentCategories()
			.then( res => {
				this.setState({
					parents: res,
					view: 'home',
					loading: false
				})
			})
	}

	getCategoryList( slug ) {
		API.getCategory( slug )
			.then( res => {
				const categoryID = _.first( res ).id

				API.getSubCategories( categoryID )
					.then( res => {

						// Update URL
						const current = _.compact( window.location.pathname.split( '/' ) ).pop()

						if( current !== slug ) {
							const nextURL = window.location.href + slug
							history.pushState( null, null, nextURL )
						}

						this.setState({
							categories: res,
							view: 'list',
							loading: false
						})
					})
			})
	}

	getCategory( slug ) {
		API.getCategory( slug )
			.then( res => {

				if ( res.length > 0 ) {

					// Update URL
					const current = _.compact( window.location.pathname.split( '/' ) ).pop()

					if( current !== slug ) {
						const nextURL = window.location.href + '/' + slug
						history.pushState( null, null, nextURL )
					}

					this.setState({
						category: res,
						view: 'category',
						loading: false
					})
				}
			})
	}

	goToSubpage(e) {
		e.preventDefault()

		const slug = e.currentTarget.getAttribute( 'data-slug' )

		this.setState({
			loading: true
		})

		this.getCategory( slug )
	}

	goToList(e) {
		e.preventDefault()

		const slug = e.currentTarget.getAttribute( 'data-slug' )

		this.setState({
			loading: true
		})

		this.getCategoryList( slug )
	}

	preloadComplete() {
		cookies.set( 'preloaded', 'yes', { path: '/' } )

		this.setState({
			preloading: false,
			animated: true
		})
	}

	orientationListener() {

		window.addEventListener("orientationchange", function() {
		    
			if ( screen.orientation.angle === 90 ) {
				confirmAlert({
				  customUI: ({ onClose }) => {
				    return (
				      <div className='custom-ui' style={{ textAlign: 'center' }}>
				        <p>Saumausmassavalitsin on optimoitu käyttämään pystysuunnassa.</p>
				        <p>Ole hyvä ja käännä päätelaite.</p>
				        <img src={ rotateURL } style={{ width: 19 }} />
				      </div>
				    )
				  }
				})
			}

			if ( screen.orientation.angle === 0 ) {
				location.reload();
			}
		});
	}

	render() {
		const _this = this
		const state = this.state
		const view = state.view
		const params = this.props.match.params

		// Parent categories
		const parents = this.state.parents
		const showParents = !!parents.length && ( view === 'home' )

		// Categories
		const categories = this.state.categories
		const showCategoryList = !!categories.length && ( view === 'list' )
		const parent = params.parent

		// Single category
		const category = this.state.category
		const showCategory = !!category.length && ( view === 'category' )

		// Loader
		const ajax = this.state.loading
		const page_load = ( !showParents &&  !showCategoryList && !showCategory )
		const showLoader = ajax || page_load

		// Styles & loader
		const containerStyles = ajax ? { opacity: 0.7 } : {}
		const containerClass = page_load ? '' : 'page-container'
		const preloading = this.state.preloading
		const animated = this.state.animated

		// Footer
		const showFooter = ( view === 'home' )

		return (
			<div className={ containerClass }>
		        { !preloading &&
		        	<div>
		        		<Header />
				        <div id="content">
							<Transition in={ this.state.fadeIn } timeout={ duration }>
								{(state) => (
									<div>
									{ showParents &&
										<CategoryList
											categories={ parents }
											view={ view }
											ajax={ ajax }
											onSelect={ _this.goToList }
										/>
									}
									{ showCategoryList &&
										<CategoryList
											categories={ categories }
											view={ view }
											ajax={ ajax }
											onSelect={ _this.goToSubpage }
										/>
									}
									{ showCategory &&
										<Category categories={ category } />
									}
									{ showLoader &&
										<div className="loader-gif">
										    <img src={ loaderURL } alt="Loader" />
										</div>
									}
									</div>
								)}
							</Transition>
						</div>
					</div>
				}
				{ preloading &&
					<Preload isComplete={ _this.preloadComplete } />
				}
				{ showFooter &&
					<Footer preloading={ preloading } animated={ animated } />
				}
        	</div>
		)
	}
}

export default Main
