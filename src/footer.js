import React from 'react'
import { Row, Col, TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap'
import classnames from 'classnames'
import InfoModal from './info-modal'

const footerLogo = smv_config.URL.theme + 'img/footer-logo.jpg'
const footerIcon = smv_config.URL.theme + 'img/footer-icon.jpg'
const footerText = smv_config.URL.theme + 'img/footer-text.jpg'

class Footer extends React.Component {

	constructor( props ) {
		super( props )

		this.state = {
			modal: false,
			activeTab: '1'
		}

		this.showModal = this.showModal.bind( this )
		this.onCloseModal = this.onCloseModal.bind( this )
		this.toggle = this.toggle.bind( this )
		this.decodeHtml = this.decodeHtml.bind( this )
	}

	showModal( e ) {
		e.preventDefault()

		this.setState({
			modal: !this.state.modal
		})
	}

	toggle( tab ) {
		if ( this.state.activeTab !== tab ) {
			this.setState({
				activeTab: tab
			})
		}
	}

	onCloseModal() {
		this.setState({
			modal: false
		})
	}

	decodeHtml( html ) {
	    let txt = document.createElement( 'textarea' )
	    txt.innerHTML = html
	    return txt.value
	}

	render() {
		const { preloading, animated } = this.props
		
		let containerClass = 'container-fluid footer-container'
		containerClass += preloading ? ' preloading' : ''
		containerClass += animated ? ' animated' : ''

		return (
			<footer id="colophon" className={ containerClass }>

				<div href="#" className="footer-information" >
					<img src={ footerLogo } className="footer-logo" alt="" />
					<img src={ footerText } className="footer-text" alt="" />
					<img src={ footerIcon } className="footer-icon" onClick={ this.showModal } alt="" />
				</div>

				<InfoModal type="text" isOpen={ this.state.modal } onClose={ this.onCloseModal }>
					<div>
						<Nav pills>
							<NavItem>
								<NavLink
									className={ classnames( { active: this.state.activeTab === '1' } ) }
									onClick={ () => { this.toggle('1') } }>
									Käyttöohjeet
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink 
									className={ classnames( { active: this.state.activeTab === '2' } ) }
									onClick={ () => { this.toggle('2') } }>
									Käyttöehdot
								</NavLink>
							</NavItem>
						</Nav>
						<TabContent activeTab={ this.state.activeTab }>
							<TabPane tabId="1">
								<Row>
									<Col sm="12" dangerouslySetInnerHTML={ { __html: smv_preload_pages['kayttoohjeet'] } }></Col>
								</Row>
							</TabPane>
							<TabPane tabId="2">
								<Row>
									<Col sm="12" dangerouslySetInnerHTML={ { __html: smv_preload_pages['kayttoehdot'] } }></Col>
								</Row>
							</TabPane>
						</TabContent>
					</div>
				</InfoModal>
		    </footer>
		)
	}
}

export default Footer