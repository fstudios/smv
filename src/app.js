import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { instanceOf } from 'prop-types'
import Main from './main'
import _ from 'lodash'

// Load the application styles
require('../scss/style.scss')

class App extends Component {

	constructor( props ) {
		super( props )
	}

	componentDidMount() {
		// update view height on resize
		window.addEventListener( 'resize', _.debounce( this.setViewHeight, 100 ) )

		// update initial view height
		this.setViewHeight()
	}

	setViewHeight() {
		// required to get the actual viewport height, instead of vh units
		// will take into account the URL bar in mobile browsers
		let vh = window.innerHeight * 0.01
		document.documentElement.style.setProperty( '--vh', `${vh}px` )
	}

	render() {
		return (
		    <div className="app-container">
		    	<BrowserRouter>
			        <Switch>
						<Route exact path={ smv_config.path } component={ Main } />
			            <Route exact path={ smv_config.path + ':parent' } component={ Main } />
			            <Route exact path={ smv_config.path + ':parent/:category' } component={ Main } />
			        </Switch>
		        </BrowserRouter>
		    </div>
		)
	}	
}

export default App
