import React, { Component } from 'react'
import { Link } from "react-router-dom"
import Transition from 'react-transition-group/Transition'

const duration = 0
const defaultStyles = { transition: 'all 650ms ease-in-out', opacity: 0, width: '101.5%', left: '-0.75%' }
const enterStyles = Object.assign( {}, defaultStyles, { opacity: 1, width: '100%', left: '0%' })

const transitionStyles = {
	exited: defaultStyles,
	exiting: defaultStyles,
	entering: defaultStyles,
	entered: enterStyles
}

class CategoryList extends Component {

	constructor( props ) {
		super( props )

		this.state = {
			fadeIn: false
		};
	}

	componentDidMount() {
		ga('send', {
			hitType: 'pageview',
			page: window.location.pathname
		});

		this.setState({
			fadeIn: true
		})
	}

	render() {
		const { categories, onSelect, ajax, view } = this.props
		const containerStyles = ajax ? { opacity: 0.7 } : {}
		const containerClass = 'category-container ' + view

		return (
			<Transition in={ this.state.fadeIn } timeout={ duration }>
				{( state ) => (
					<div className={ containerClass } style={ Object.assign( {}, transitionStyles[state], containerStyles ) }>
						{ categories.map( ( item, index ) => {
							return (
								<Link
									key={ index }
									to={ '/'+item.slug }
									onClick={ onSelect }
									data-slug={ item.slug }
									className="vertical-layout-link"
									style={ { backgroundImage: 'url(' + item.category_list_image_src + ')' } }>
									<div className="vertical-layout-text">{ item.name } <span className="chevron">›</span></div>
								</Link>
							)
						})}
					</div>
				)}
			</Transition>
		)
	}
}

export default CategoryList
