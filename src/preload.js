import React from "react"

const preloadImageURL = smv_config.URL.theme + 'img/preloader-image-50.jpg'
const minDuration = 3000

class Preload extends React.Component {

	constructor( props ) {
		super( props )

		this.state = {
			progress: 0
		}
	}

	componentDidMount() {
		const _this = this

		const images = smv_preload_images
		const start = Date.now()
		const increment = Math.ceil( 100 / images.length )
		let counter = 0

		// Preload category list images
		images.forEach(( image ) => {
		    const img = new Image()

			img.addEventListener( 'load', ( ev ) => {

				let progress = _this.state.progress + increment

				_this.setState({
					progress: progress
				})

				counter++

				// Set preload complete when all images are loaded
				if( counter === images.length ) {
					const duration = Date.now() - start

					if ( duration > minDuration ) {
						_this.props.isComplete()	

					} else {
						const timeout = minDuration - duration

						setTimeout( () => {
							_this.props.isComplete()
						}, timeout)
					}
				}

			}, false)

		    img.src = image
		})
	}

	render() {
		let backgroundImage = 'url(' + preloadImageURL + ')'
		let width = this.state.progress + '%'

		return (
			<div className="preload-container" style={{ backgroundImage: backgroundImage }}>
				<div className="loader">
					<div className="progress-bar" style={{ width: width }}></div>
				</div>
			</div>
		)
	}
}

export default Preload
