import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { render } from 'react-dom'
import App from './app'

const routes = (
    <Router>
        <Route path="/" component={ App } />
    </Router>
)

render(
    ( routes ), document.getElementById( 'page' )
)