<?php
/**
 * The main template file
 *
 * @package WordPress
 * @subpackage Saumausmassavalitsin
 * @since Saumausmassavalitsin 1.0
 */
 ?>
 <!DOCTYPE html>
 <html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <title>Saumausmassavalitsin</title>

        <!-- Favicon -->
        <link rel="icon" type="image/png" sizes="64x64" href="<?php echo get_template_directory_uri()?>/img/Icon-App-64x64@1x.png">
        <!-- Apple/Safari icon -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri()?>/img/Icon-App-180x180@1x.png">
        <!-- Square Windows tiles -->
        <meta name="msapplication-square70x70logo" content="<?php echo get_template_directory_uri()?>/img/Icon-App-72x72@1x.png">
        <meta name="msapplication-square150x150logo" content="<?php echo get_template_directory_uri()?>/img/Icon-App-150x150@1x.png">
        <meta name="msapplication-square310x310logo" content="<?php echo get_template_directory_uri()?>/img/Icon-App-310x310@1x.png">
        <!-- Rectangular Windows tile -->
        <meta name="msapplication-wide310x150logo" content="<?php echo get_template_directory_uri()?>/img/Icon-App-310x150@1x.png">
        <!-- Windows tile theme color -->
        <meta name="msapplication-TileColor" content="#08415c">

        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div id="page" class="hfeed site app-background"></div>

        <script>

        	<?php
        		/*
					Output JSON data for preload image URLs
				*/
				$args = array("hide_empty" => 0);
				$categories = get_categories( $args );
				$images = [];

				foreach ( $categories as $category ) {
					$category_image = get_term_meta( $category->term_id, SMV_PREFIX . 'category_image', true);
					$category_list_image = get_term_meta( $category->term_id, SMV_PREFIX . 'category_list_image', true);

					if ( !empty( $category_image ) ) {
						array_push( $images, $category_image );
					}

					if ( !empty( $category_list_image ) ) {
						array_push( $images, $category_list_image );
					}
				}

				echo "var smv_preload_images = " . json_encode( $images ) . ";" . "\n\n";

				
				/*
					Output JSON data for terms and instruction pages
				*/
				$text = [];
				$terms = new WP_Query( array( 'pagename' => 'kayttoehdot' ) );
				$instructions = new WP_Query( array( 'pagename' => 'kayttoohjeet' ) );
				
				if ( $terms->have_posts() ) {
					while ( $terms->have_posts() ) {
						$terms->the_post();
						global $post;

						$text[ $post->post_name ] = wpautop( get_the_content() );
					}
					wp_reset_postdata();
				}

				if ( $instructions->have_posts() ) {
					while ( $instructions->have_posts() ) {
						$instructions->the_post();
						global $post;

						$text[ $post->post_name ] = wpautop( get_the_content() );
					}
					wp_reset_postdata();
				}

				echo "var smv_preload_pages = " . json_encode( $text ) . ";" . "\n";
        	?>

        </script>

        <?php wp_footer(); ?>
        <script>
        	// Google Analytics tracking - Fallback when plugin is not installed
        	var ga = ga || function( a, b ) {
        		console.log('ga tracking not set up correctly', a, b);
			};
        </script>
    </body>
</html>